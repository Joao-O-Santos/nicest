.PHONY: tests

all: clean tests


clean:
	-rm tests/log

tests:
	Rscript tests/tests.R | tee tests/log
