# Nicest



## Description

A project template for using CI/CD, and DevOps concepts, for science.



## Tutorial

- To get all functions: `source("./nicest.R")` or `source(/path/to/nicest.R)`
- For in-text reporting of `afex_aov` or `afex_mixed` objects run: `nicest(object_name)`
- For in-text reporting of `emmeans` CIs run: `nice_cis(emmeans_object_name)`
- You can then copy paste the outputs to `Rmd` or other markdown files
  to be rendered with `pandoc`.



## WARNING

These functions are meant for in-text reporting of results (e.g., versus
just printing tables). Meaning, the typical use involves describing the
results in writing and pasting the statistics at the end of the line.
Using the functions interactively in `RMarkdown` is discouraged as the
text descriptions may no longer adequately describe the results once
they change.*



## COPYING

This project is licensed under the GPL-v3.0-only license, please ignore
any indication in the GitLab UI that specifies otherwise (e.g., any
button reading "GPL-v3.0-or-later" should read "GPL-v3.0-only").
